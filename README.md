# Assignment 
Task is to create a CI/CD environment consist of one Host Tool for CI/CD and a target machine, which is a dev environment. The Host CI/CD should able to deploy a code from Code Repository to Dev Environment. 

```mermaid
graph LR
A[Host CI/CD] -- Fetch from --> C(Git Remote Repo)
A -- Deploy to --> B((Dev Environment))

```

# Solution 
A solution is based on `AWS platform` VMs and `Ubuntu 18.04` OS. The Details of the VMs are as below. 

| VM Name  | Public IP  | Private IP | Port | Instance Type | 
| ------------- | ------------- |------------- |------------- |------------- | 
| Development Machine  | 35.154.145.222  | 172.31.8.209 | 30000 | t2.micro | 
| Jenkins-Docker  | 13.127.45.104  | 172.31.12.55 | 8080 | t2.micro | 

For CI/CD, have consider `Jenkins ver 2.138.2`

# Solution Repositories 
The Solution is spread over two repositories. 
1. **Setting up of Jenkins :** To quick start, you can set up a docker and pull the Jenkin's working directory. The benefit of this approach is, you will get a ready to execute Deployment Job. Ofcourse, you will need to modify some configuration if you want to go with your own dev environment. 
2. **Code Repository :** This is a code Repository of "Emoji App". An application to play along. 

# Solution Setup 
## Jenkins Setup 
This solution is based on the popular CI/CD tool, Jenkins. You can setup your own Jenkin server as mention in [Digitalocean doc](https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-18-04) OR use the [Jenkins Doc root](https://gitlab.com/jidneshb/pru-jenkins) + Docker approach to quick start the Job. Below are the steps of these two approaches in brief.

### Manual Jenkins configuration 
> **Consideration:** *- A Jenkins is setup and tested OK as per above Digitalocean link. 
                     - Basic Jenkins Plug-in are also available like git, Build Timeout etc
                     - A Dev Machine and Jenkins server are able to connect each other over ssh 
                     - Keep a ssh key handy with you, which you have used while testing the ssh connectivity*

1. Install a required plug-in 
For this setup, as you need a connectivity over ssh to deploy your build on Dev machine. We need two functionalities 
- Copy over SSH 
- Remote command execution over SSH 
[Publish Over SSH](https://wiki.jenkins.io/display/JENKINS/Publish+Over+SSH+Plugin), this plugin is giving us both these functionalities. Go to **Manage Jenkins -> Manage Plugins**, in Available section search for SSH and check ""Publish Over SSH" and click on Install plugin. 

2. Once you have installed above plugin, you can go and configure it in **Jenkins -> configuration -> Publish over SSH** section. Please refer below image. Copy paste your key in "key" section. Or you can mention a PATH of your key file. Give an appropriate name to identifiy the Machine, we will need this name in Job configuration. Please do have a look at *Document/ExeComSSH.png* Image. 

![](Document/Publish%20over%20SSH.png)

3. Create a Job 
To create a Job, go to "New Item", Give a name, and select "Free Style Project" and click on "OK".
In Configuration form, Enter Description, Choose source code Management as Git. 
As we have to push a Build to remote machine, we need a mechanism to identify a build. So have used here an additional feature **"Check out to sub-directory"**, and to decide the name of the directory have used Jenkins inbuild varible `$BUILD_TAG`.
![](Document/Proj_form_Gen.png)
![](Document/Jenkins_SCM.png)

>*** To test the changes, have used my repository. Feel free to change it as per your convenience ***

You can execute this build either manually or periodically. 
> *There are other ways also, like Poll SCM, Webhooks but this assignment is currently limited to above two approches only.*

For periodic, have set `H H/6 * * * `, which will trigger the build in every 6 Hrs. 

![](Document/Build%20Trigger.png)

If you need, you can clean up the Jenkins workspace before every build by choosing an option availble in "Build Environment".

The logic of our execution is mentioned in "Build" section. 
+ First we create a `TAR.GZ` archieve 
+ Then we push this archieve to remote Dev machine 
+ On Remote machine, we extract and run the build 

have a look at below screen shot for your understanding.
![](Document/ExeShell.png)
![](Document/ExeComSSH.png)

The Detailed steps of execution on remote machine are available [here](https://gitlab.com/jidneshb/pru-jenkins/tree/28ee24d97409a66fe9e78901d6a75cc650c3d709/Build-Scripts).

And Save this configuration.

4. This has set up a JOB for your Dev Deployment.

### Dockerized approch
> **Consideration:** A Ubuntu 18.04 machine. With Git Installed.
This is a simple and straight forward approach. Where you just need to have a docker available on your system, with the [pru-jenkins](/pru-jenkins) repo downloaded. 

```sh 
$sudo apt install apt-transport-https ca-certificates curl software-properties-common 
``` 

```sh 
$curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 
```

```sh
$sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" 
```

```sh 
$sudo apt update 
```

```sh
$apt-cache policy docker-ce 
```

```sh
$sudo apt install docker-ce 
```

```sh
$sudo systemctl status docker 
```

```sh
$sudo usermod -aG docker ${USER} 
```

```sh
$git clone https://gitlab.com/jidneshb/pru-jenkins.git
```

```sh
$docker run -itd -p 8080:8080 -p 50000:50000 -v /home/ubuntu/pru-jenkins:/var/jenkins_home jenkins/jenkins:lts 
```

Now, go to your public IP / localhost, port 8080 on your browser, to access the Jenkins. 

> To access the Jenkins, deployed by Docker & Work-Doc approach is having `jidnesh\jidnesh`; as access credentials.


## Further possible modification if time permit 
+ Currently have used Docker containerization approach for Jenkins setup, however there is still a scope of doing the same for Application. We can use `node` docker Image and deploy the application in Docker container.

+ With Dockerization in place, we can use ECS/Kubernetes to deploy the App and CI/CD in a cluster. By doing this we will overcome the need of seperate VM's for these two application.
 